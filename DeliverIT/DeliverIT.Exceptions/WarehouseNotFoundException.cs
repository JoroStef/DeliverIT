﻿using DeliverIT.Exceptions.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Exceptions
{
    public class WarehouseNotFoundException : Exception
    {
        public WarehouseNotFoundException(string message)
            : base(message)
        {

        }

        public WarehouseNotFoundException(int id)
            : base(string.Format(ErrorMessages.NOT_FOUND, "Warehouse", "id", id))

        {

        }

    }
}
