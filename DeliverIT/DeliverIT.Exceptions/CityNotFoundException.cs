﻿using DeliverIT.Exceptions.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Exceptions
{
    public class CityNotFoundException : Exception
    {
        public CityNotFoundException(int id)
            : base(string.Format(ErrorMessages.NOT_FOUND, "City", "id", id))
        {

        }
    }
}
