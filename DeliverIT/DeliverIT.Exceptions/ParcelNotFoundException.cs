﻿using DeliverIT.Exceptions.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Exceptions
{
    public class ParcelNotFoundException : Exception
    {
        public ParcelNotFoundException(int id)
            : base(string.Format(ErrorMessages.NOT_FOUND,"Parcel", "id", id))
        {

        }
    }
}
