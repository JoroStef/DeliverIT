﻿using DeliverIT.Exceptions.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Exceptions
{
    public class DuplicateEmailAddressException : Exception
    {
        public DuplicateEmailAddressException(string personType, string duplicateEmail) :
            base(string.Format(ErrorMessages.DUPLICATE_EMAIL_ADDRESS, personType, duplicateEmail))
        {

        }
    }
}
