﻿using DeliverIT.Exceptions.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Exceptions
{
    public class CategoryNotFoundException : Exception
    {
        public CategoryNotFoundException(int id) 
            : base(string.Format(ErrorMessages.NOT_FOUND, "Category", "id", id))
        {

        }

        public CategoryNotFoundException(string name)
            : base(string.Format(ErrorMessages.NOT_FOUND, "Category", "name", name))
        {

        }
    }
}
