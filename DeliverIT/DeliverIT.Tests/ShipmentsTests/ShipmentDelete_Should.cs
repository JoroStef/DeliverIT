﻿using DeliverIT.Data;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ShipmentsTests
{
    [TestClass]
    public class ShipmentDelete_Should
    {
        [TestMethod]
        public void Delete_ShouldThrow_WhenInvalidIdIsPassed()
        {
            // Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                //Act && Assert
                Assert.ThrowsException<ShipmentNotFoundException>(() => shipmentService.Delete(-1));
            }
        }

        [TestMethod]
        public void Delete_ShouldRemoveCorrect_WhenValidIdIsPassed()
        {
            // Arrange

            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                int delShipmentId = context.Shipments.First().ShipmentId;

                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                // Act

                shipmentService.Delete(delShipmentId);
                List<int> seededShipmentsId_after = context.Shipments.Select(sh => sh.ShipmentId).ToList();

                // Assert

                Assert.IsFalse(seededShipmentsId_after.Contains(delShipmentId));
            }
        }


    }
}
