﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using DeliverIT.Services.Models.InputDTOs;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ShipmentsTests
{
    [TestClass]
    public class ShipmentUpdate_Should
    {
        [TestMethod]
        public void Update_ShouldThrow_WhenInvalidIdIsPassed()
        {
            // Arrange

            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                //Act && Assert
                Assert.ThrowsException<ShipmentNotFoundException>(() => shipmentService.Update(-1, null));
            }
        }

        [TestMethod]
        public void Update_ShouldThrow_WhenInvalidDatesArePassed()
        {
            // Arrange

            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                // 1. Departure date is after arrival date
                // 2. Departure date is before Now
                // 3. Arrival date is before Now
                List<DateTime?> departureDates = new List<DateTime?>()
                {
                   DateTime.Now.AddDays(3),
                   DateTime.Now.AddDays(-1),
                   null
                };
                List<DateTime?> arrivalDates = new List<DateTime?>()
                {
                   DateTime.Now.AddDays(1),
                   null,
                   DateTime.Now.AddDays(-1)
                };

                for (int i = 0; i < arrivalDates.Count; i++)
                {
                    InputShipment input = new InputShipment()
                    {
                        ArrivalDate = arrivalDates[i],
                        DepartureDate = departureDates[i]
                    };

                    // Act & Assert
                    Assert.ThrowsException<ArgumentException>(() => shipmentService.Update(1, input));
                }
            }
        }

        [TestMethod]
        public void Update_ShouldThrow_WhenInvalidWarehouseIdIsPassed()
        {
            // Arrange

            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                InputShipment input = new InputShipment()
                {
                    WarehouseId = 10
                };

                // Act & Assert
                Assert.ThrowsException<ArgumentException>(() => shipmentService.Update(1, input));
            }
        }

        [TestMethod]
        public void Update_ShouldThrow_WhenChangeStatusToOnTheWayAndNoParcels()
        {
            // Arrange

            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                Shipment shipment = new Shipment()
                {
                    ShipmentId = 1,
                    ShipmentStatusId = 1,
                    WarehouseId = 1
                };

                context.Database.EnsureDeleted();
                context.Add(shipment);
                context.SaveChanges();

                InputShipment input = new InputShipment()
                {
                    ShipmentStatusId = 2
                };

                // Act & Assert
                Assert.ThrowsException<ShipmentStatusException>(() => shipmentService.Update(1, input));
            }
        }

        [TestMethod]
        public void Update_ShouldReturnObject_WhenValidInput()
        {
            // Arrange

            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                Util.SeedDataInMemoryDatabase(context);

                InputShipment input = new InputShipment()
                {
                    DepartureDate = DateTime.Now.AddDays(1),
                    ArrivalDate = DateTime.Now.AddDays(2),
                    WarehouseId = 1,
                    ShipmentStatusId = 2
                };

                // Act
                var newShipmentDTO = shipmentService.Update(1, input);

                // Assert
                Assert.AreEqual(input.ArrivalDate.Value.ToString("yyyy/MM/dd"), newShipmentDTO.ArrivalDate);
                Assert.AreEqual(input.DepartureDate.Value.ToString("yyyy/MM/dd"), newShipmentDTO.DepartureDate);
                Assert.AreEqual("on the way", newShipmentDTO.Status);
                Assert.AreEqual(input.WarehouseId, newShipmentDTO.DestinationWarehouse.Id);
            }
        }

    }
}
