﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ShipmentsTests
{
    [TestClass]
    public class ShipmentRemoveParcel_Should
    {
        [TestMethod]
        public void ShipmentRemoveParcel_ShouldThrow_WhenInvalidParcelIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                //Act && Assert
                Assert.ThrowsException<ParcelNotFoundException>(() => shipmentService.RemoveParcel(1, -1));
            }
        }

        [TestMethod]
        public void ShipmentRemoveParcel_ShouldThrow_WhenInvalidShipmentIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                //Act && Assert
                Assert.ThrowsException<ShipmentNotFoundException>(() => shipmentService.RemoveParcel(-1, 1));
            }
        }

        [TestMethod]
        public void ShipmentRemoveParcel_ShouldRemoveCorrectParcel_WhenValidInput()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                // Act
                shipmentService.RemoveParcel(1, 2);

                Shipment shipment = context.Shipments.FirstOrDefault(sh => sh.ShipmentId == 1);
                var parcels = shipment.Parcels.Select(p => p.ParcelId);

                // Assert
                Assert.IsFalse(parcels.Contains(2));
            }
        }

        [TestMethod]
        public void ShipmentRemoveParcel_ShouldThrow_WhenStatusIsNotPreparing()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {

                var warehouseService = new WarehouseService(context);

                var shipmentService = new ShipmentService(context, warehouseService, new CustomerService(context));

                Parcel parcel = new Parcel()
                {
                    CategoryId = 1,
                    CustomerId = 1,
                    WarehouseId = 1,
                    Weight = 10,
                    ShipmentId =1
                };

                Shipment shipment = new Shipment()
                {
                    ShipmentStatusId = 2,
                    WarehouseId = 1,
                };

                context.Database.EnsureDeleted();
                context.Parcels.Add(parcel);
                context.Shipments.Add(shipment);
                context.SaveChanges();

                // Act && Assert
                Assert.ThrowsException<ArgumentException>(() => shipmentService.RemoveParcel(1, 1));
            }
        }
    }
}
