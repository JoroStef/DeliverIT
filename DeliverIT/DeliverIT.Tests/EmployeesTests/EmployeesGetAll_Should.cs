﻿using DeliverIT.Data;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.EmployeesTests
{
    [TestClass]
    class EmployeesGetAll_Should
    {
        [TestMethod]
        public void GetAll_ShouldReturnSameCollectionAsExpected()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using(var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var allEmployees = context.Employees.ToList();

                var sut = new EmployeeService(context);

                //Act
                var result = sut.GetAll();

                //Assert
                Assert.AreEqual(allEmployees.Count(), result.Count());
                Assert.AreEqual(allEmployees.First().EmployeeId, result.First().Id);
                Assert.AreEqual(allEmployees.Last().EmployeeId, result.Last().Id);
            }
        }
    }
}
