﻿using DeliverIT.Data;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using DeliverIT.Services.Models.OutputDTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.CustomersTests
{
    [TestClass]
    public class CustomersGet_Should
    {
        [TestMethod]
        public void GetById_ShouldThrowCustomerNotFoundException_WhenInvalidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var customers = context.Customers.ToList();

                var invalidId = customers.Max(c => c.CustomerId) + 1;
                var customerService = new CustomerService(context);

                //Act && Assert
                Assert.ThrowsException<CustomerNotFoundException>(() => customerService.Get(invalidId));
            }
        }

        [TestMethod]
        public void GetById_ShouldReturnCorrectCustomer_WhenValidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var customers = context.Customers.ToList();

                var validId = new Random().Next(customers.Min(c=>c.CustomerId), customers.Max(c => c.CustomerId));
                var customerService = new CustomerService(context);
                //Act
                var customer = customerService.Get(validId);

                //Assert
                Assert.IsTrue(validId == customer.Id);
            }
        }

        [TestMethod]
        public void GetById_ShouldReturnCorrectInstanceType_WhenValidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var customers = context.Customers;

                var validId = new Random().Next(customers.Min(c=>c.CustomerId), customers.Max(c => c.CustomerId));
                var customerService = new CustomerService(context);

                //Act
                var customer = customerService.Get(validId);

                //Assert
                Assert.IsInstanceOfType(customer, typeof(CustomerDTO));
            }
        }
    }
}
