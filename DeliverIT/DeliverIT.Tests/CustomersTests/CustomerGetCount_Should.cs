﻿using DeliverIT.Data;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.CustomersTests
{
    [TestClass]
    public class CustomerGetCount_Should
    {
        [TestMethod]
        public void GetCount_ShouldReturnCustomersCount()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);
                var customersCount = context.Customers.Count();
                var sut = new CustomerService(context);

                //Act && Assert
                Assert.AreEqual(customersCount, sut.GetCountCustomers());
            }
        }
    }
}
