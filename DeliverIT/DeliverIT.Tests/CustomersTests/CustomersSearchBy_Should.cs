﻿using DeliverIT.Data;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using DeliverIT.Services.Models.OutputDTOs;
using DeliverIT.Services.QueryObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.CustomersTests
{
    [TestClass]
    public class CustomersSearchBy_Should
    {
        [TestMethod]
        public void SearchBy_ShouldReturnAllCustomers_WhenNoCriteriaSpecified()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);
                var allCustomers = context.Customers.ToList();
                var customerService = new CustomerService(context);

                //Act
                var customers = customerService.SearchBy(new CustomerFilteringCriterias());

                //Assert
                Assert.AreEqual(allCustomers.Count(), customers.Count());
                Assert.AreEqual(allCustomers.First().CustomerId, customers.First().Id);
                Assert.AreEqual(allCustomers.Last().CustomerId, customers.Last().Id);
            }
        }

        [TestMethod]
        public void SearchBy_ShouldReturnEmptyCollection_WhenInvalidCriteriaIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var criterias = new CustomerFilteringCriterias() { FirstName = "InvalidFirstName" };
                var customerService = new CustomerService(context);

                //Act
                var customers = customerService.SearchBy(criterias);

                //Assert
                Assert.IsFalse(customers.Any());
            }
        }

        [TestMethod]
        public void SearchBy_ShouldReturnFilteredByFirstNameCollection_WhenOnlyValidFirstNameIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var criterias = new CustomerFilteringCriterias() { FirstName = "John" };
                var filteredByFirstName = context.Customers.Where(c => c.FirstName == criterias.FirstName).ToList();

                var customerService = new CustomerService(context);

                //Act
                var customers = customerService.SearchBy(criterias);

                //Assert
                Assert.AreEqual(filteredByFirstName.Count(), customers.Count());
                Assert.AreEqual(filteredByFirstName.First().CustomerId, customers.First().Id);
                Assert.AreEqual(filteredByFirstName.Last().CustomerId, customers.Last().Id);
            }
        }

        [TestMethod]
        public void SearchBy_ShouldReturnAllOccurenciesWithPartOrFullEmail_WhenValidEmailIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);
                var criterias = new CustomerFilteringCriterias() { Email = "johnatanasov@gmail.com" };

                var filteredByEmail = context.Customers
                 .Where(cust => cust.Email == criterias.Email);

                var customerService = new CustomerService(context);

                //Act
                var customers = customerService.SearchBy(criterias);

                //Assert
                Assert.AreEqual(filteredByEmail.Count(), customers.Count());
                Assert.AreEqual(filteredByEmail.First().CustomerId, customers.First().Id);
                Assert.AreEqual(filteredByEmail.Last().CustomerId, customers.Last().Id);
            }
        }

        [TestMethod]
        public void SearchBy_ShouldReturnAllOccurenciesWithStateContainingKeyword_WhenValidKeywordIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);
                var criterias = new CustomerFilteringCriterias() { Keyword = ""};

                var filteredByKeyword = context.Customers
                 .Where(cust => cust.FirstName.Contains(criterias.Keyword)
                 || cust.LastName.Contains(criterias.Keyword)
                 || cust.Email.Contains(criterias.Keyword));

                var customerService = new CustomerService(context);
                //Act
                var customers = customerService.SearchBy(criterias);

                //Assert
                Assert.AreEqual(filteredByKeyword.Count(), customers.Count());
                Assert.AreEqual(filteredByKeyword.First().CustomerId, customers.First().Id);
                Assert.AreEqual(filteredByKeyword.Last().CustomerId, customers.Last().Id);
            }
        }

        [TestMethod]
        public void SearchBy_ShouldReturnCorrectCollectionType()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var customerService = new CustomerService(context);


                //Act
                var customers = customerService.SearchBy(new CustomerFilteringCriterias());

                //Assert
                Assert.IsInstanceOfType(customers, typeof(IEnumerable<CustomerDTO>));
            }
        }
    }
}
