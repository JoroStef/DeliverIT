﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using DeliverIT.Services.Models.InputDTOs;
using DeliverIT.Services.Models.OutputDTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ParcelsTests
{
    [TestClass]
    public class ParcelsUpdate_Should
    {
        [TestMethod]
        public void Update_ShouldThrowArgumentNullException_WhenNullInputParcelIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new ParcelService(context);

                //Act && Assert
                Assert.ThrowsException<ArgumentNullException>(() => sut.Update(1, null));
            }
        }

        [TestMethod]
        public void Update_ShouldThrowParcelNotFoundException_WhenInvalidIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new ParcelService(context);

                var inputParcel = new InputParcel()
                {
                    CustomerId = 2,
                    WarehouseId = 1,
                    ShipmentId = 1,
                    CategoryId = 1,
                    Weight = 3
                };

                var invalidId = context.Parcels.Max(p => p.ParcelId) + 1;
                //Act && Assert
                Assert.ThrowsException<ParcelNotFoundException>(() => sut.Update(invalidId, inputParcel));
            }
        }

        [TestMethod]
        public void Update_ShouldThrowCustomerNotFoundException_WhenInvalidCustomerIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new ParcelService(context);

                var inputParcel = new InputParcel()
                {
                    CustomerId = 20,
                    WarehouseId = 1,
                    ShipmentId = 1,
                    CategoryId = 1,
                    Weight = 3
                };

                var validId = context.Parcels.Max(p => p.ParcelId);
                //Act && Assert
                Assert.ThrowsException<CustomerNotFoundException>(() => sut.Update(validId, inputParcel));
            }
        }

        [TestMethod]
        public void Update_ShouldThrowWarehouseNotFoundException_WhenInvalidWarehouseIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new ParcelService(context);

                var inputParcel = new InputParcel()
                {
                    CustomerId = 2,
                    WarehouseId = 10,
                    ShipmentId = 1,
                    CategoryId = 1,
                    Weight = 3
                };

                var validId = context.Parcels.Max(p => p.ParcelId);
                //Act && Assert
                Assert.ThrowsException<WarehouseNotFoundException>(() => sut.Update(validId, inputParcel));
            }
        }

        [TestMethod]
        public void Update_ShouldThrowShipmentNotFoundException_WhenInvalidShipmentIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new ParcelService(context);

                var inputParcel = new InputParcel()
                {
                    CustomerId = 2,
                    WarehouseId = 1,
                    ShipmentId = 10,
                    CategoryId = 1,
                    Weight = 3
                };

                var validId = context.Parcels.Max(p => p.ParcelId);
                //Act && Assert
                Assert.ThrowsException<ShipmentNotFoundException>(() => sut.Update(validId, inputParcel));
            }
        }

        [TestMethod]
        public void Update_ShouldThrowCategoryNotFoundException_WhenInvalidCategoryIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new ParcelService(context);

                var inputParcel = new InputParcel()
                {
                    CustomerId = 2,
                    WarehouseId = 1,
                    ShipmentId = 1,
                    CategoryId = 10,
                    Weight = 3
                };

                var validId = context.Parcels.Max(p => p.ParcelId);
                //Act && Assert
                Assert.ThrowsException<CategoryNotFoundException>(() => sut.Update(validId, inputParcel));
            }
        }

        [TestMethod]
        public void Update_ShouldThrowArgumentException_WhenInvalidWeightIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new ParcelService(context);

                var inputParcel = new InputParcel()
                {
                    CustomerId = 2,
                    WarehouseId = 1,
                    ShipmentId = 1,
                    CategoryId = 1,
                    Weight = -5
                };

                var validId = context.Parcels.Max(p => p.ParcelId);
                //Act && Assert
                Assert.ThrowsException<ArgumentException>(() => sut.Update(validId, inputParcel));
            }
        }

        [TestMethod]
        public void Update_ShouldReturnCorrectInstanceType_WhenValidArgumentsArePassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var validId = new Random().Next(context.Parcels.Min(p => p.ParcelId), context.Parcels.Max(p => p.ParcelId));
                var sut = new ParcelService(context);

                var inputParcel = new InputParcel()
                {
                    CustomerId = 2,
                    WarehouseId = 1,
                    ShipmentId = 1,
                    CategoryId = 1,
                    Weight = 3
                };

                //Act
                var result = sut.Update(validId, inputParcel);

                //Assert
                Assert.IsInstanceOfType(result, typeof(ParcelDTO));
            }
        }

        [TestMethod]
        public void Update_ShouldReturnCorrectlyUpdatedInstance_WhenValidArgumentsArePassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new ParcelService(context);

                var inputParcel = new InputParcel()
                {
                    CustomerId = 2,
                    WarehouseId = 1,
                    ShipmentId = 1,
                    CategoryId = 1,
                    Weight = 3
                };

                var parcel = new Parcel()
                {
                    Category = new Category(),
                    Purchaser = new Customer(),
                    Warehouse = new Warehouse(),
                    Shipment = new Shipment() { ArrivalDate = DateTime.Now.AddDays(5) }
                };

                parcel.Category.Name = "in progress";
                parcel.Purchaser.FirstName = "Ivan";
                parcel.Purchaser.LastName = "Kolev";
                parcel.Weight = 3;
                parcel.Warehouse.Address = new Address();
                parcel.Warehouse.Address.City = new City();
                parcel.Warehouse.Address.City.Country = new Country();
                parcel.Warehouse.Address.City.Name = "Sofia";
                parcel.Warehouse.Address.City.Country.Name = "Bulgaria";
                parcel.Warehouse.Address.Street = "bul. Botevgradsko shose";

                var parcelDTO = new ParcelDTO(parcel);

                var validId = context.Parcels.Max(p => p.ParcelId);

                //Act
                var updatedParcel = sut.Update(validId, inputParcel);

                //Assert
                Assert.AreEqual(updatedParcel.Weight, parcelDTO.Weight);
                Assert.AreEqual(updatedParcel.ParcelLocation, parcelDTO.ParcelLocation);
                Assert.AreEqual(updatedParcel.PurchaserName, parcelDTO.PurchaserName);
                Assert.AreEqual(updatedParcel.Category, parcelDTO.Category);
            }
        }
    }
}
