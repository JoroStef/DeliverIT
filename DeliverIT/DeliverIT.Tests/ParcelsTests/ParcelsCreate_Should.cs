﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using DeliverIT.Services.Models.InputDTOs;
using DeliverIT.Services.Models.OutputDTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ParcelsTests
{
    [TestClass]
    public class ParcelsCreate_Should
    {
        [TestMethod]
        public void Create_ShouldThrowArgumentNullException_WhenInvalidInputParcelIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new ParcelService(context);

                //Act && Assert
                Assert.ThrowsException<ArgumentNullException>(() => sut.Create(null));
            }
        }

        [TestMethod]
        public void Create_ShouldThrowCustomerNotFoundException_WhenInvalidCustomerIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new ParcelService(context);

                var inputParcel = new InputParcel()
                {
                    CategoryId = 1,
                    WarehouseId = 1,
                    ShipmentId = 1,
                    CustomerId = 20,
                    Weight = 4
                };


                //Act && Assert
                Assert.ThrowsException<CustomerNotFoundException>(() => sut.Create(inputParcel));
            }
        }

        [TestMethod]
        public void Create_ShouldThrowCategoryNotFoundException_WhenInvalidCategoryIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new ParcelService(context);

                var inputParcel = new InputParcel()
                {
                    CategoryId = 10,
                    WarehouseId = 1,
                    ShipmentId = 1,
                    CustomerId = 2,
                    Weight = 4
                };


                //Act && Assert
                Assert.ThrowsException<CategoryNotFoundException>(() => sut.Create(inputParcel));
            }
        }

        [TestMethod]
        public void Create_ShouldThrowShipmentNotFoundException_WhenInvalidShipmentIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new ParcelService(context);

                var inputParcel = new InputParcel()
                {
                    CategoryId = 1,
                    WarehouseId = 1,
                    ShipmentId = 10,
                    CustomerId = 2,
                    Weight = 4
                };


                //Act && Assert
                Assert.ThrowsException<ShipmentNotFoundException>(() => sut.Create(inputParcel));
            }
        }

        [TestMethod]
        public void Create_ShouldThrowWarehouseNotFoundException_WhenInvalidWarehouseIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new ParcelService(context);

                var inputParcel = new InputParcel()
                {
                    CategoryId = 1,
                    WarehouseId = 10,
                    ShipmentId = 1,
                    CustomerId = 2,
                    Weight = 4
                };


                //Act && Assert
                Assert.ThrowsException<WarehouseNotFoundException>(() => sut.Create(inputParcel));
            }
        }

        [TestMethod]
        public void Create_ShouldThrowArgumentNullException_WhenNoWarehouseIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new ParcelService(context);

                var inputParcel = new InputParcel()
                {
                    CategoryId = 1,
                    WarehouseId = null,
                    ShipmentId = 1,
                    CustomerId = 2,
                    Weight = 4
                };


                //Act && Assert
                Assert.ThrowsException<ArgumentNullException>(() => sut.Create(inputParcel));
            }
        }

        [TestMethod]
        public void Create_ShouldThrowArgumentNullException_WhenNoCustomerIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new ParcelService(context);

                var inputParcel = new InputParcel()
                {
                    CategoryId = 1,
                    WarehouseId = 1,
                    ShipmentId = 1,
                    CustomerId = null,
                    Weight = 4
                };


                //Act && Assert
                Assert.ThrowsException<ArgumentNullException>(() => sut.Create(inputParcel));
            }
        }

        [TestMethod]
        public void Create_ShouldThrowArgumentNullException_WhenNoCategoryIdIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new ParcelService(context);

                var inputParcel = new InputParcel()
                {
                    CategoryId = null,
                    WarehouseId = 1,
                    ShipmentId = 1,
                    CustomerId = 1,
                    Weight = 4
                };


                //Act && Assert
                Assert.ThrowsException<ArgumentNullException>(() => sut.Create(inputParcel));
            }
        }

        [TestMethod]
        public void CreateShould_CreateCorrectInstance_WhenValidParametersArePassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new ParcelService(context);

                var inputParcel = new InputParcel()
                {
                    CategoryId = 1,
                    WarehouseId = 1,
                    ShipmentId = 1,
                    CustomerId = 2,
                    Weight = 4
                };
                //Act
                var parcel = sut.Create(inputParcel);

                var lastCreatedParcel = context.Parcels.Last();

                //Assert
                Assert.AreEqual(parcel.Id, lastCreatedParcel.ParcelId);
            }
        }

        [TestMethod]
        public void CreateShould_CreateCorrectInstanceType_WhenValidParametersArePassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new ParcelService(context);

                var inputParcel = new InputParcel()
                {
                    CategoryId = 1,
                    WarehouseId = 1,
                    ShipmentId = 1,
                    CustomerId = 2,
                    Weight = 4
                };
                //Act
                var parcel = sut.Create(inputParcel);

                //Assert
                Assert.IsInstanceOfType(parcel, typeof(ParcelDTO));
            }
        }

        [TestMethod]
        public void Create_ShouldReturnCorrectInstance_ValidInputModelIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new ParcelService(context);

                var inputParcel = new InputParcel()
                {
                    CategoryId = 1,
                    WarehouseId = 1,
                    ShipmentId = 1,
                    CustomerId = 1,
                    Weight = 4
                };

                //Act
                var createdParcel = sut.Create(inputParcel);


                //Act && Assert
                Assert.AreEqual(createdParcel.Id, context.Parcels.Max(p=>p.ParcelId));
            }
        }
    }
}
