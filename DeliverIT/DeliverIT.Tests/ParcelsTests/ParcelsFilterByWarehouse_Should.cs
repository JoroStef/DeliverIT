﻿using DeliverIT.Data;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ParcelsTests
{
    [TestClass]
    public class ParcelsFilterByWarehouse_Should
    {
        [TestMethod]
        public void FilterByWarehouse_ShouldThrowArgumentException_WhenInvalidCityIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using(var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new ParcelService(context);

                //Act && Assert
                Assert.ThrowsException<ArgumentException>(() => sut.FilterByWarehouse("Sliven", "Bulgaria"));
            }
        }

        [TestMethod]
        public void FilterByWarehouse_ShouldThrowArgumentException_WhenInvalidCountryIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new ParcelService(context);

                //Act && Assert
                Assert.ThrowsException<ArgumentException>(() => sut.FilterByWarehouse("Sofia", "Armenia"));
            }
        }

        [TestMethod]
        public void FilterByWarehouse_ShouldThrowArgumentException_WhenNoWarehousesAreFoundInPassedCity()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var sut = new ParcelService(context);

                //Act && Assert
                Assert.ThrowsException<ArgumentException>(() => sut.FilterByWarehouse("Sofia", "Germany"));
            }
        }

        [TestMethod]
        public void FilterByWarehouse_ShouldReturnFilteredByCityCollection_WhenValidCityIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var filteredByCity = context.Parcels.Where(p => p.Warehouse.Address.City.Name == "Sofia");

                var sut = new ParcelService(context);

                //Act
                var result = sut.FilterByWarehouse("Sofia", null);

                //Assert
                Assert.AreEqual(filteredByCity.Count(), result.Count());
                Assert.AreEqual(filteredByCity.First().ParcelId, result.First().Id);
                Assert.AreEqual(filteredByCity.Last().ParcelId, result.Last().Id);
            }
        }

        [TestMethod]
        public void FilterByWarehouse_ShouldReturnFilteredByCountryCollection_WhenValidCountryIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var filteredByCity = context.Parcels.Where(p => p.Warehouse.Address.City.Country.Name == "Bulgaria");

                var sut = new ParcelService(context);

                //Act
                var result = sut.FilterByWarehouse(null, "Bulgaria");

                //Assert
                Assert.AreEqual(filteredByCity.Count(), result.Count());
                Assert.AreEqual(filteredByCity.First().ParcelId, result.First().Id);
                Assert.AreEqual(filteredByCity.Last().ParcelId, result.Last().Id);
            }
        }

        [TestMethod]
        public void FilterByWarehouse_ShouldReturnFilteredByCountryAndCityCollection_WhenValidCountryAndCityArePassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var filteredByCity = context.Parcels
                    .Where(p => p.Warehouse.Address.City.Country.Name == "Bulgaria"
                    && p.Warehouse.Address.City.Name == "Sofia");

                var sut = new ParcelService(context);

                //Act
                var result = sut.FilterByWarehouse("Sofia", "Bulgaria");

                //Assert
                Assert.AreEqual(filteredByCity.Count(), result.Count());
                Assert.AreEqual(filteredByCity.First().ParcelId, result.First().Id);
                Assert.AreEqual(filteredByCity.Last().ParcelId, result.Last().Id);
            }
        }
    }
}
