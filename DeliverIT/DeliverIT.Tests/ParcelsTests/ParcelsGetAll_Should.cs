﻿using DeliverIT.Data;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ParcelsTests
{
    [TestClass]
    public class ParcelsGetAll_Should
    {
        [TestMethod]
        public void GetAll_ShouldReturnAllParcels()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using(var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var allParcels = context.Parcels.ToList();

                var sut = new ParcelService(context);

                //Act
                var result = sut.GetAll();

                //Assert
                Assert.AreEqual(allParcels.Count(), result.Count());
                Assert.AreEqual(allParcels.First().ParcelId, result.First().Id);
                Assert.AreEqual(allParcels.Last().ParcelId, result.Last().Id);
            }
        }
    }
}
