﻿using DeliverIT.Data;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Strings;
using DeliverIT.Services;
using DeliverIT.Services.Models.OutputDTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.CountriesTests
{
    [TestClass]
    public class CountriesGetCities_Should
    {
        [TestMethod]
        public void GetCountryCities_ShouldThrowCountryNotFoundException_WhenInvalidCountryIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var countryService = new CountryService(context);

                //Act && Assert
                Assert.ThrowsException<CountryNotFoundException>(() => countryService.GetCities("Armenia"));
            }
        }

        [TestMethod]
        public void GetCountryCities_ShouldThrowArgumentException_WhenCountryDoesNotHaveCities()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var countryService = new CountryService(context);

                //Act && Assert 
                Assert.ThrowsException<ArgumentException>(() => countryService.GetCities("Greece"));
            }
        }

        [TestMethod]
        public void GetCountryCities_ShouldReturnCorrectInstanceType_WhenValidCountryIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var countryService = new CountryService(context);

                //Act
                var citiesDTOs = countryService.GetCities("Bulgaria");

                //Assert
                Assert.IsInstanceOfType(citiesDTOs, typeof(CountryCitiesDTO));
            }
        }

        [TestMethod]
        public void GetCountryCities_ShouldReturnCorrectCollectionOfCities_WhenValidCountryIsPassed()
        {
            //Arrange
            var options = Util.GetDbContextOptions(Strings.TEST_DB);

            using (var context = new DeliverITDbContext(options))
            {
                Util.SeedDataInMemoryDatabase(context);

                var country = context.Countries.FirstOrDefault(c => c.Name == "Germany");
                var cities = context.Cities
                    .Where(c => c.CountryId == country.CountryId)
                    .Select(c => c.Name);

                var countryService = new CountryService(context);

                //Act
                var methodCountries = countryService.GetCities("Germany");


                //Assert
                CollectionAssert.AreEqual(cities.ToList(), methodCountries.Cities.ToList());
            }
        }
    }
}
