﻿using DeliverIT.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Data
{
    public interface IDeliverITDbContext
    {
         DbSet<Warehouse> Warehouses { get; set; }

         DbSet<Address> Addresses { get; set; }

         DbSet<Employee> Employees { get; set; }

         DbSet<Customer> Customers { get; set; }

         DbSet<Shipment> Shipments { get; set; }

         DbSet<ShipmentStatus> ShipmentStatuses { get; set; }

         DbSet<Parcel> Parcels { get; set; }

         DbSet<Category> Categories { get; set; }

         DbSet<City> Cities { get; set; }

         DbSet<Country> Countries { get; set; }

        int SaveChanges();
    }
}
