﻿using DeliverIT.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Data
{
    public class DeliverITDbContext : DbContext, IDeliverITDbContext
    {
        public DeliverITDbContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<Warehouse> Warehouses { get; set; }

        public DbSet<Address> Addresses { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Shipment> Shipments { get; set; }

        public DbSet<ShipmentStatus> ShipmentStatuses { get; set; }

        public DbSet<Parcel> Parcels { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<City> Cities { get; set; }

        public DbSet<Country> Countries { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Fluent API
            modelBuilder.Entity<Parcel>()
                .HasOne(p => p.Shipment)
                .WithMany(sh => sh.Parcels)
                .HasForeignKey(p => p.ShipmentId)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Customer>().HasData(SeedCustomers());

            modelBuilder.Entity<Employee>().HasData(SeedEmployees());

            modelBuilder.Entity<Parcel>().HasData(SeedParcels());

            modelBuilder.Entity<Country>().HasData(SeedCountries());

            modelBuilder.Entity<City>().HasData(SeedCities());

            modelBuilder.Entity<Address>().HasData(SeedAddresses());

            modelBuilder.Entity<ShipmentStatus>().HasData(SeedShipmentStatuses());

            modelBuilder.Entity<Category>().HasData(SeedCategories());

            modelBuilder.Entity<Warehouse>().HasData(SeedWarehouses());

            modelBuilder.Entity<Shipment>().HasData(SeedShipments());

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            return base.SaveChanges();
        }

        private List<ShipmentStatus> SeedShipmentStatuses()
        {
            return new List<ShipmentStatus>()
            {
                new ShipmentStatus(){ ShipmentStatusId = 1, Status = "preparing"},
                new ShipmentStatus(){ ShipmentStatusId = 2, Status = "on the way"},
                new ShipmentStatus(){ ShipmentStatusId = 3, Status = "completed"}
            };
        }

        private List<Country> SeedCountries()
        {
            return new List<Country>()
            {
                new Country(){CountryId = 1, Name = "Bulgaria" },
                new Country(){CountryId = 2, Name = "United Kingdom"},
                new Country(){CountryId = 3, Name = "Germany"}
            };
        }

        private List<City> SeedCities()
        {
            return new List<City>()
            {
                new City(){CityId = 1, Name = "Sofia", CountryId = 1},
                new City(){CityId = 2, Name = "London", CountryId = 2},
                new City(){CityId = 3, Name = "Berlin", CountryId = 3},
                new City(){CityId = 4, Name = "Varna", CountryId = 1},
                new City(){CityId = 5, Name = "Manchester", CountryId = 2},
                new City(){CityId = 6, Name = "Hamburg", CountryId = 3}
            };
        }

        private List<Address> SeedAddresses()
        {
            return new List<Address>()
            {
                new Address(){AddressId = 1, Street = "bul. Botevgradsko shose", CityId = 1},
                new Address(){AddressId = 2, Street = "bul. Primorski", CityId = 4},
                new Address(){AddressId = 3, Street = "Abbey Road", CityId = 2},
                new Address(){AddressId = 4, Street = "Cheetham Hill Road", CityId = 5},
                new Address(){AddressId = 5, Street = "Tauentzienstrasse", CityId = 3},
                new Address(){AddressId = 6, Street = "Deichstrasse", CityId = 6},
            };
        }

        private List<Customer> SeedCustomers()
        {
            return new List<Customer>()
            {
              new Customer() { CustomerId = 1, FirstName = "Dragan", LastName = "Ivanov", AddressId = 2, Email = "draganivanov@gmail.com" },
              new Customer() { CustomerId = 2, FirstName = "Ivan", LastName = "Kolev", AddressId = 1, Email = "ivankolev@gmail.com" },
              new Customer() { CustomerId = 3, FirstName = "John", LastName = "Atanasov", AddressId = 4, Email = "johnatanasov@gmail.com"}
            };
        }

        private List<Employee> SeedEmployees()
        {
            return new List<Employee>()
            {
                new Employee() { EmployeeId = 1, FirstName = "Atanas", LastName = "Kolev", AddressId = 1, Email = "atanaskolev@gmail.com"},
                new Employee() { EmployeeId = 2, FirstName = "Kolio", LastName = "Manev", AddressId = 1, Email = "koliomanev@gmail.com"},
                new Employee() { EmployeeId = 3, FirstName = "Petar", LastName = "Andreev", AddressId = 1, Email = "petarandreev@gmail.com"}
            };
        }

        private List<Parcel> SeedParcels()
        {
            return new List<Parcel>()
            {
                new Parcel() {ShipmentId = 1, ParcelId = 1, CategoryId = 1, CustomerId = 1, WarehouseId= 1, Weight = 6},
                new Parcel() {ShipmentId = 1, ParcelId = 2, CategoryId = 2, CustomerId = 3, WarehouseId= 1, Weight = 4},
                new Parcel() {ShipmentId = 1, ParcelId = 3, CategoryId = 2, CustomerId = 2, WarehouseId= 1, Weight = 5}
            };
        }

        private List<Category> SeedCategories()
        {
            return new List<Category>()
            {
                new Category() {CategoryId = 1, Name = "electronics"},
                new Category() {CategoryId = 2, Name = "clothing" },
                new Category() {CategoryId = 3, Name = "medical" },
            };
        }

        private List<Warehouse> SeedWarehouses()
        {
            return new List<Warehouse>()
            {
                new Warehouse() { WarehouseId = 1, AddressId = 1},
                new Warehouse() { WarehouseId = 2, AddressId = 3},
                new Warehouse() { WarehouseId = 3, AddressId = 5},
                new Warehouse() { WarehouseId = 4, AddressId = 2},
            };
        }

        private List<Shipment> SeedShipments()
        {
            return new List<Shipment>()
            {
                new Shipment()
                {
                    ShipmentId = 1,
                    DepartureDate = DateTime.Now.AddDays(1),
                    ArrivalDate = DateTime.Now.AddDays(2),
                    WarehouseId = 1,
                    ShipmentStatusId = 1
                }
            };
        }
    }
}
