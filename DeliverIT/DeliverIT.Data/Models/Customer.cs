﻿using DeliverIT.Data.Models;
using DeliverIT.Exceptions.Messages;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DeliverIT.Data
{

    /// <author>
    /// Mihail Popov
    /// </author>
    public class Customer
    {

        [Key]
        public int CustomerId { get; set; }

        [Required(ErrorMessage = ErrorMessages.FIRST_NAME_REQUIRED)]
        [StringLength(25, MinimumLength = 2, ErrorMessage = "{0} lenght should be between {2} and {1}")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = ErrorMessages.LAST_NAME_REQUIRED)]
        [StringLength(25, MinimumLength = 2, ErrorMessage = "Last name should be between {2} and {1}")]
        public string LastName { get; set; }

        [Required(ErrorMessage = ErrorMessages.EMAIL_ADDRESS_REQUIRED)]
        [EmailAddress(ErrorMessage = ErrorMessages.INVALID_EMAIL_FORMAT)]
        public string Email { get; set; }

        [Required(ErrorMessage = ErrorMessages.ADDRESS_ID_REQUIRED)]
        public int AddressId { get; set; }

        public Address Address { get; set; }

        public ICollection<Parcel> Parcels { get; set; } = new List<Parcel>();
    }
}
