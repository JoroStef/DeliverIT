﻿using DeliverIT.Exceptions.Messages;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DeliverIT.Data.Models
{
    /// <author>
    /// Georgi Stefanov
    /// </author>
    public class Address
    {
        public Address()
        {
            this.Employees = new List<Employee>();
            this.Customers = new List<Customer>();
        }

        [Key]
        public int AddressId { get; set; }

        [Required(ErrorMessage = ErrorMessages.STREET_REQUIRED)]
        [MaxLength(30, ErrorMessage = "Street name should be up to {0} symbols long")]
        public string Street { get; set; }

        [Required(ErrorMessage = ErrorMessages.CITY_ID_REQUIRED)]
        public int CityId { get; set; }

        public City City { get; set; }

        public IEnumerable<Employee> Employees { get; set; }

        public IEnumerable<Customer> Customers { get; set; }
    }
}
