﻿using DeliverIT.Exceptions.Messages;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DeliverIT.Data.Models
{
    public class ShipmentStatus
    {
        public ShipmentStatus()
        {
            this.Shipments = new List<Shipment>();
        }

        [Key]
        public int ShipmentStatusId { get; set; }

        [Required(ErrorMessage = ErrorMessages.SHIPMENT_STATUS_NAME_REQUIRED)]
        public string Status { get; set; }

        public ICollection<Shipment> Shipments { get; set; }
    }
}
