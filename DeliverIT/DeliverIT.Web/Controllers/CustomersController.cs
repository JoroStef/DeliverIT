﻿using DeliverIT.Data;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Messages;
using DeliverIT.Services.Interfaces;
using DeliverIT.Services.Models.InputDTOs;
using DeliverIT.Services.Models.OutputDTOs;
using DeliverIT.Services.QueryObjects;
using DeliverIT.Web.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliverIT.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        /// <summary>
        /// The customer service
        /// </summary>
        private readonly ICustomerService customerService;
        /// <summary>
        /// The authentication helper
        /// </summary>
        private readonly IAuthHelper authHelper;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomersController"/> class.
        /// </summary>
        /// <param name="customerService">The customer service.</param>
        /// <param name="authHelper">The authentication helper.</param>
        public CustomersController(ICustomerService customerService, IAuthHelper authHelper)
        {
            this.customerService = customerService;
            this.authHelper = authHelper;
        }

        /// <summary>
        /// Gets customer by specified identifier.
        /// </summary>
        /// <param name="authentication">The user authentication.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public IActionResult Get([FromHeader] string authentication, int id)
        {
            try
            {
                if(customerService.IsAuthenticated(authentication))
                {
                     return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                var customer = customerService.Get(id);
                return Ok(customer);
            }
            catch (CustomerNotFoundException cnfe)
            {
                return NotFound(cnfe.Message);
            }
            catch(UnauthorizedAccessException uae)
            {
                return Unauthorized(uae.Message);
            }
        }

        /// <summary>
        /// Gets the customers count.
        /// </summary>
        /// <returns></returns>
        [HttpGet("count")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult GetCustomersCount()
        {
            return Ok(customerService.GetCountCustomers());
        }

        /// <summary>
        /// Register the specified customer.
        /// </summary>
        /// <param name="customer">The input customer.</param>
        /// <returns></returns>
        [HttpPost("")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Post([FromBody] InputCustomer customer)
        {

            try
            {
                var createdCustomer = customerService.Create(customer);
                return Created("uri", createdCustomer);
            }
            catch(ArgumentNullException ane)
            {
                return BadRequest(ane.Message);
            }
            catch (ArgumentException ae)
            {
                return BadRequest(ae.Message);
            }
            catch(DuplicateEmailAddressException deae)
            {
                return BadRequest(deae.Message);
            }
            catch(AddressNotFoundException anfe)
            {
                return BadRequest(anfe.Message);
            }
        }

        /// <summary>
        /// Updates the specified customer by authentication.
        /// </summary>
        /// <param name="authentication">The user authentication.</param>
        /// <param name="customer">The input customer.</param>
        /// <returns></returns>
        [HttpPut("")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public IActionResult Put([FromHeader] string authentication, [FromBody] InputCustomer customer)
        {
            try
            {
                var customerToUpdate = authHelper.TryGetCustomer(authentication);
                var updatedCustomer = customerService.Update(customerToUpdate, customer);
                return Ok(updatedCustomer);
            }
            catch(ArgumentNullException ane)
            {
                return BadRequest(ane.Message);
            }
            catch (CustomerNotFoundException cnfe)
            {
                return NotFound(cnfe.Message);
            }
            catch(AddressNotFoundException anfe)
            {
                return BadRequest(anfe.Message);
            }
            catch(DuplicateEmailAddressException deae)
            {
                return BadRequest(deae.Message);
            }
            catch(UnauthorizedAccessException uae)
            {
                return Unauthorized(uae.Message);
            }
        }

        /// <summary>
        /// Deletes customer by specified identifier.
        /// </summary>
        /// <param name="authentication">The user authentication.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public IActionResult Delete([FromHeader] string authentication, int id)
        {
            try
            {
                if(customerService.IsAuthenticated(authentication))
                {
                     return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                var customer = authHelper.TryGetEmployee(authentication);

                var isDeleted = customerService.Delete(id);

                if (isDeleted)
                {
                    return NoContent();
                }

                return NotFound();
            }
            catch (UnauthorizedAccessException uae)
            {
                return Unauthorized(uae.Message);
            }
        }

        /// <summary>
        /// Gets all customers or customers filtered by specified criterias.
        /// </summary>
        /// <param name="authentication">The user authentication.</param>
        /// <param name="filteringCriterias">The filtering criterias.</param>
        /// <param name="filter">The pagination filter</param>
        /// <returns></returns>
        [HttpGet("")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public IActionResult SearchBy([FromHeader] string authentication,
            [FromQuery] CustomerFilteringCriterias filteringCriterias, [FromQuery] PaginationFilter filter)
        {
            try
            {
                if(customerService.IsAuthenticated(authentication))
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                var customers = customerService.SearchBy(filteringCriterias);

                if(!customers.Any())
                {
                    return NoContent();
                }

                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                return Ok(new PagedResponse<CustomerDTO>().GetPaginated(customers, validFilter.PageNumber, validFilter.PageSize));
            }
            catch (ArgumentException ae)
            {
                return NotFound(ae.Message);
            }
            catch(UnauthorizedAccessException uae)
            {
                return Unauthorized(uae.Message);
            }
        }

        /// <summary>
        /// Gets all customers with part of or full email.
        /// </summary>
        /// <param name="authentication">The user authentication.</param>
        /// <param name="email">The email to search for.</param>
        /// <returns></returns>
        [HttpGet("email/{email}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public IActionResult GetByEmail([FromHeader] string authentication, string email, [FromQuery] PaginationFilter filter)
        {
            try
            {
                if(customerService.IsAuthenticated(authentication))
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                var customers = customerService.SearchByPartEmail(email);

                if(!customers.Any())
                {
                    return NoContent();
                }

                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                return Ok(new PagedResponse<CustomerDTO>().GetPaginated(customers, validFilter.PageNumber, validFilter.PageSize));
            }
            catch (ArgumentException ae)
            {
                return NotFound(ae.Message);
            }
            catch(UnauthorizedAccessException uae)
            {
                return Unauthorized(uae.Message);
            }
        }
    }
}
