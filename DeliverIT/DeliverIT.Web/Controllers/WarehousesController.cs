﻿using DeliverIT.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliverIT.Exceptions;
using DeliverIT.Web.Helpers;
using DeliverIT.Services.QueryObjects;
using DeliverIT.Services.Models.OutputDTOs;
using DeliverIT.Exceptions.Messages;

namespace DeliverIT.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [ApiController]
    [Route("api/[controller]")]
    public class WarehousesController : ControllerBase
    {
        /// <summary>
        /// The warehouse service
        /// </summary>
        private readonly IWarehouseService warehouseService;
        /// <summary>
        /// The authentication helper
        /// </summary>
        private readonly IAuthHelper authHelper;
        private readonly ICustomerService customerService;

        /// <summary>
        /// Initializes a new instance of the <see cref="WarehousesController"/> class.
        /// </summary>
        /// <param name="warehouseService">The warehouse service.</param>
        /// <param name="authHelper">The authentication helper.</param>
        public WarehousesController(IWarehouseService warehouseService, IAuthHelper authHelper, ICustomerService customerService)
        {
            this.warehouseService = warehouseService;
            this.authHelper = authHelper;
            this.customerService = customerService;
        }

        /// <summary>Gets all.</summary>
        /// <param name="filter">The pagination filter</param>
        /// <returns>
        ///   <br />
        /// </returns>
        [HttpGet("")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public IActionResult GetAll([FromQuery] PaginationFilter filter)
        {
            var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);

            var allWarehouses = this.warehouseService.GetAll();

            if (allWarehouses.Any())
            {
                return Ok(new PagedResponse<WarehouseDTO>().GetPaginated(allWarehouses, validFilter.PageNumber, validFilter.PageSize));
            }
            else
            {
                return NoContent();
            }
        }

        /// <summary>Gets the specified identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <param name="authentication">The authentication.</param>
        /// <returns>
        ///   <br />
        /// </returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public IActionResult Get(int id, [FromHeader] string authentication)
        {
            try
            {
                if(customerService.IsAuthenticated(authentication))
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                authHelper.TryGetEmployee(authentication);

                var warehouse = warehouseService.Get(id);
                return Ok(warehouse);
            }
            catch (WarehouseNotFoundException e)
            {
                return NotFound(e.Message);
            }
            catch (UnauthorizedAccessException e)
            {
                return Unauthorized(e.Message);
            }
        }
    }
}
