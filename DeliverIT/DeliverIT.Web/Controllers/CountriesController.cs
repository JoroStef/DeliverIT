﻿using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Messages;
using DeliverIT.Services.Interfaces;
using DeliverIT.Services.Models.OutputDTOs;
using DeliverIT.Services.QueryObjects;
using DeliverIT.Web.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliverIT.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [Route("api/[controller]")]
    [ApiController]
    public class CountriesController : ControllerBase
    {
        /// <summary>
        /// The country service
        /// </summary>
        private readonly ICountryService countryService;
        /// <summary>
        /// The authentication helper
        /// </summary>
        private readonly IAuthHelper authHelper;
        private readonly ICustomerService customerService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CountriesController"/> class.
        /// </summary>
        /// <param name="countryService">The country service.</param>
        /// <param name="authHelper">The authentication helper.</param>
        public CountriesController(ICountryService countryService, IAuthHelper authHelper, ICustomerService customerService)
        {
            this.countryService = countryService;
            this.authHelper = authHelper;
            this.customerService = customerService;
        }

        /// <summary>
        /// Gets all countries.
        /// </summary>
        /// <param name="authentication">The user authentication.</param>
        /// <param name="filter">The pagination filter</param>
        /// <returns></returns>
        [HttpGet("")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public IActionResult GetAll([FromHeader] string authentication, [FromQuery] PaginationFilter filter)
        {
            try
            {

                if(customerService.IsAuthenticated(authentication))
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                this.authHelper.TryGetEmployee(authentication);

                var allCountries = countryService.GetAll();

                if(!allCountries.Any())
                {
                    return NoContent();
                }

                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                return Ok(new PagedResponse<CountryDTO>().GetPaginated(allCountries, validFilter.PageNumber, validFilter.PageSize));
            }
            catch(ArgumentException ae)
            {
                return NotFound(ae.Message);
            }
            catch(UnauthorizedAccessException uae)
            {
                return Unauthorized(uae.Message);
            }
        }

        /// <summary>
        /// Gets country by specified identifier.
        /// </summary>
        /// <param name="authentication">The user authentication.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public IActionResult Get([FromHeader] string authentication, int id)
        {
            try
            {
                if (customerService.IsAuthenticated(authentication))
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                this.authHelper.TryGetEmployee(authentication);

                var country = countryService.Get(id);
                return Ok(country);
            }
            catch (CountryNotFoundException cnfe)
            {
                return NotFound(cnfe.Message);
            }
            catch(UnauthorizedAccessException uae)
            {
                return Unauthorized(uae.Message);
            }
        }

        /// <summary>
        /// Gets country cities by country name.
        /// </summary>
        /// <param name="authentication">The user authentication.</param>
        /// <param name="countryName">Name of the country.</param>
        /// <returns></returns>
        [HttpGet("cities")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public IActionResult Get([FromHeader] string authentication, string countryName)
        {
            try
            {
                if (customerService.IsAuthenticated(authentication))
                {
                    return StatusCode(403, ErrorMessages.FORBIDDEN_ACCESS);
                }
                this.authHelper.TryGetEmployee(authentication);

                var countryCities = countryService.GetCities(countryName);
                return Ok(countryCities);
            }
            catch(CountryNotFoundException cnfe)
            {
                return NotFound(cnfe.Message);
            }
            catch(ArgumentException ae)
            {
                return NotFound(ae.Message);
            }
            catch(UnauthorizedAccessException uae)
            {
                return Unauthorized(uae.Message);
            }
        }
    }
}
