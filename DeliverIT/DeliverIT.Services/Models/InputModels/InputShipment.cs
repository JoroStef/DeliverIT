﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.Models.InputDTOs
{
    public class InputShipment
    {
        public DateTime? DepartureDate { get; set; }

        public DateTime? ArrivalDate { get; set; }

        public int? ShipmentStatusId { get; set; }

        public int? WarehouseId { get; set; }
    }
}
