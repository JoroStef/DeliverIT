﻿using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace DeliverIT.Services.Models.OutputDTOs
{
    public class CityDTO
    {
        public CityDTO(City city)
        {
            this.Name = city.Name;
            this.Country = city.Country.Name;
            this.Id = city.CityId;
        }

        [JsonIgnore]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Country { get; set; }
        
    }
}
