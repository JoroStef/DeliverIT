﻿using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace DeliverIT.Services.Models.OutputDTOs
{
    public class ShipmentDTO
    {
        public ShipmentDTO(Shipment shipment)
        {
            this.Id = shipment.ShipmentId;

            if (shipment.DepartureDate != null)
            {
                this.DepartureDate = shipment.DepartureDate.Value.ToString("yyyy/MM/dd");
            }
            if (shipment.ArrivalDate != null)
            {
                this.ArrivalDate = shipment.ArrivalDate.Value.ToString("yyyy/MM/dd");
            }            

            this.Status = shipment.ShipmentStatus.Status;
        }

        public ShipmentDTO(Shipment shipment, WarehouseDTO warehouseDTO)
            : this(shipment)
        {
            this.DestinationWarehouse = warehouseDTO;
        }

        public ShipmentDTO(Shipment shipment, WarehouseDTO warehouseDTO, List<ParcelDTO> parcelsDTO)
            : this(shipment, warehouseDTO)
        {
            this.Parcels = parcelsDTO;
        }

        [JsonIgnore]
        public int Id { get; set; }

        public string DepartureDate { get; set; }

        public string ArrivalDate { get; set; }

        public string Status { get; set; }

        public List<ParcelDTO> Parcels { get; set; } = new List<ParcelDTO>();

        public WarehouseDTO DestinationWarehouse { get; set; }
    }
}
