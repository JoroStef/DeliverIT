﻿using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace DeliverIT.Services.Models.OutputDTOs
{
    public class WarehouseDTO
    {
        public WarehouseDTO(Warehouse warehouse)
        {
            this.Id = warehouse.WarehouseId;

            this.FullAddress = 
                warehouse.Address.Street + ", " +
                warehouse.Address.City.Name + ", " +
                warehouse.Address.City.Country.Name;
        }

        [JsonIgnore]
        public int Id { get; set; }

        public string FullAddress { get; set; }
    }
}
