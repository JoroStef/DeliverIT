﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Messages;
using DeliverIT.Services.Interfaces;
using DeliverIT.Services.Models;
using DeliverIT.Services.Models.InputDTOs;
using DeliverIT.Services.Models.OutputDTOs;
using DeliverIT.Services.QueryObjects;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Services
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="DeliverIT.Services.Interfaces.ICustomerService" />
    public class CustomerService : ICustomerService
    {
        /// <summary>
        /// The database context
        /// </summary>
        private IDeliverITDbContext dbContext;
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerService"/> class.
        /// </summary>
        /// <param name="dbContext">The database context.</param>
        public CustomerService(IDeliverITDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        /// <summary>
        /// Register the specified customer.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <returns>The registered customer as CustomerDTO object</returns>
        /// <exception cref="ArgumentException"></exception>
        public CustomerDTO Create(InputCustomer customer)
        {
            if (customer == null)
            {
                throw new ArgumentException(string.Format(ErrorMessages.FAILED_REGISTRATION, "model"));
            }

            Customer customerToAdd = new Customer();

            this.ValidateInput(customer);
            this.SetState(customerToAdd, customer);

            dbContext.Customers.Add(customerToAdd);
            dbContext.SaveChanges();

            var addedCustomer = this.GetIncludedCustomers().FirstOrDefault(c => c.CustomerId == customerToAdd.CustomerId);

            return new CustomerDTO(addedCustomer);
        }

        /// <summary>
        /// Deletes customer by specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>true if the customer is successfully deleted or false otherwise</returns>
        public bool Delete(int id)
        {
            var customer = dbContext.Customers
                .Include(c => c.Parcels)
                .FirstOrDefault(user => user.CustomerId == id);

            if (customer == null)
            {
                return false;
            }

            dbContext.Customers.Remove(customer);
            dbContext.SaveChanges();

            return true;
        }

        /// <summary>
        /// Updates the specified customer.
        /// </summary>
        /// <param name="customerToUpdate">The customer to update.</param>
        /// <param name="customer">The input customer.</param>
        /// <returns>The updated customer as CustomerDTO object</returns>
        /// <exception cref="ArgumentNullException"></exception>
        public CustomerDTO Update(Customer customerToUpdate, InputCustomer customer)
        {
            if (customer == null)
            {
                throw new ArgumentNullException(string.Format(ErrorMessages.FAILED_UPDATE, "model"));
            }
            
            this.ValidateInput(customer);
            this.UpdateState(customerToUpdate, customer);

            dbContext.SaveChanges();

            return new CustomerDTO(customerToUpdate);
        }

        /// <summary>
        /// Gets customer by specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The customer to get as CustomerDTO object</returns>
        /// <exception cref="CustomerNotFoundException"></exception>
        public CustomerDTO Get(int id)
        {
            var customer = GetIncludedCustomers().FirstOrDefault(customer => customer.CustomerId == id);

            if (customer == null)
            {
                throw new CustomerNotFoundException(id);
            }

            return new CustomerDTO(customer);
        }

        /// <summary>
        /// Gets all customers.
        /// </summary>
        /// <returns>Collection of all customers as CustomerDTO objects</returns>
        public IEnumerable<CustomerDTO> GetAll()
        {
            var customers = GetIncludedCustomers();

            return GetCustomerDTOs(customers);
        }

        /// <summary>
        /// Searches customers by specified customer criterias.
        /// </summary>
        /// <param name="criterias">The criterias.</param>
        /// <returns>
        /// Collection of all customers if no criteria is
        /// passed or customers searched by some criteria as CustomerDTO objects
        /// </returns>
        public IEnumerable<CustomerDTO> SearchBy(CustomerFilteringCriterias criterias)
        {
            IEnumerable<Customer> customers = this.GetIncludedCustomers();

            if (criterias.Keyword != null)
            {
                customers = SearchByKeyword(criterias.Keyword);
                return GetCustomerDTOs(customers);
            }

            if (criterias.FirstName != null)
            {
                customers = customers.Where(c => c.FirstName == criterias.FirstName);
            }

            if (criterias.LastName != null)
            {
                customers = customers.Where(c => c.LastName == criterias.LastName);
            }

            if (criterias.City != null)
            {
                customers = customers.Where(c => c.Address.City.Name == criterias.City);
            }

            if (criterias.Street != null)
            {
                customers = customers.Where(c => c.Address.Street == criterias.Street);
            }

            if (criterias.Country != null)
            {
                customers = customers.Where(c => c.Address.City.Country.Name == criterias.Country);
            }

            if (criterias.Email != null)
            {
                customers = customers.Where(c => c.Email == criterias.Email);
            }

            return GetCustomerDTOs(customers);
        }

        /// <summary>
        /// Searches customers by part or full email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns>Collection of all customers as CustomerDTO objects</returns>
        public IEnumerable<CustomerDTO> SearchByPartEmail(string email)
        {
            var customers = GetIncludedCustomers();

            if (email != null)
            {
                customers = customers.Where(c => c.Email.Contains(email));
            }

            return GetCustomerDTOs(customers);
        }

        /// <summary>
        /// Authenticates customer by email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns>The authenticated customer</returns>
        /// <exception cref="UnauthorizedAccessException">Invalid credentials</exception>
        public Customer AuthenticateByEmail(string email)
        {
            var customer = dbContext.Customers.FirstOrDefault(c => c.Email == email);

            if (customer == null)
            {
                throw new UnauthorizedAccessException(ErrorMessages.INVALID_CREDENTIALS);
            }

            return customer;
        }

        /// <summary>
        /// Determines whether the specified customer is authenticated.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns>
        /// true if the specified customer is authenticated, otherwise - false
        /// </returns>
        public bool IsAuthenticated(string email)
        {
            var customer = dbContext.Customers.FirstOrDefault(c => c.Email == email);

            if (customer == null)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Gets the count of the customers.
        /// </summary>
        /// <returns>integer - the count of the customers</returns>
        public int GetCountCustomers()
        {
            return dbContext.Customers.Count();
        }

        /// <summary>
        /// Gets the customer DTOs.
        /// </summary>
        /// <param name="customers">The customers.</param>
        /// <returns>Collection of the passed as argument customers as CustomerDTO objects</returns>
        private IEnumerable<CustomerDTO> GetCustomerDTOs(IEnumerable<Customer> customers)
        {
            ICollection<CustomerDTO> dtos = new List<CustomerDTO>();

            foreach (var customer in customers)
            {
                dtos.Add(new CustomerDTO(customer));
            }

            return dtos;
        }

        /// <summary>
        /// Searches customers by keyword.
        /// </summary>
        /// <param name="keyword">The keyword.</param>
        /// <returns>Collection of the customers searched by specified keyword as CustomerDTO objects</returns>
        private IEnumerable<Customer> SearchByKeyword(string keyword)
        {
            return GetIncludedCustomers().Where(cust => cust.FirstName.Contains(keyword)
                 || cust.LastName.Contains(keyword)
                 || cust.Email.Contains(keyword));
        }

        /// <summary>
        /// Gets the customers with their included relations.
        /// </summary>
        /// <returns>Collection of the customers</returns>
        private IEnumerable<Customer> GetIncludedCustomers()
        {
            var customers = dbContext.Customers
                .Include(c => c.Address)
                   .ThenInclude(a => a.City)
                      .ThenInclude(c => c.Country);

            return customers;
        }

        /// <summary>
        /// Validates the customer input.
        /// </summary>
        /// <param name="inputCustomer">The input customer.</param>
        /// <exception cref="AddressNotFoundException"></exception>
        /// <exception cref="DuplicateEmailAddressException">Customer</exception>
        private void ValidateInput(InputCustomer inputCustomer)
        {
            if (inputCustomer.AddressId!=null && dbContext.Addresses.FirstOrDefault(a => a.AddressId == inputCustomer.AddressId) == null)
            {
                throw new AddressNotFoundException(inputCustomer.AddressId.Value);
            }

            if (inputCustomer.Email != null && dbContext.Customers.FirstOrDefault(c => c.Email == inputCustomer.Email) != null)
            {
                throw new DuplicateEmailAddressException("Customer", inputCustomer.Email);
            }
        }

        /// <summary>
        /// Updates the state of the customer.
        /// </summary>
        /// <param name="customer">The customer to update.</param>
        /// <param name="inputCustomer">The input customer.</param>
        private void UpdateState(Customer customer, InputCustomer inputCustomer)
        {
            if (inputCustomer.AddressId != null)
                customer.AddressId = inputCustomer.AddressId.Value;

            if (inputCustomer.FirstName != null)
                customer.FirstName = inputCustomer.FirstName;

            if (inputCustomer.LastName != null)
                customer.LastName = inputCustomer.LastName;

            if (inputCustomer.Email != null)
                customer.Email = inputCustomer.Email;
        }

        /// <summary>
        /// Sets the state of the customer.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <param name="inputCustomer">The input customer.</param>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        private void SetState(Customer customer, InputCustomer inputCustomer)
        {
            if (inputCustomer.AddressId == null)
                throw new ArgumentNullException(string.Format(ErrorMessages.PARAMETER_REQUIRED, "Address id"));

            if (inputCustomer.FirstName == null)
                throw new ArgumentNullException(string.Format(ErrorMessages.PARAMETER_REQUIRED, "First name"));

            if (inputCustomer.LastName == null)
                throw new ArgumentNullException(string.Format(ErrorMessages.PARAMETER_REQUIRED, "Last name"));

            if (inputCustomer.Email == null)
                throw new ArgumentNullException(string.Format(ErrorMessages.PARAMETER_REQUIRED, "Email"));

            customer.AddressId = inputCustomer.AddressId.Value;
            customer.LastName = inputCustomer.LastName;
            customer.FirstName = inputCustomer.FirstName;
            customer.Email = inputCustomer.Email;
        }
    }
}
