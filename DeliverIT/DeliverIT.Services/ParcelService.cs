﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Exceptions.Messages;
using DeliverIT.Services.Interfaces;
using DeliverIT.Services.Models;
using DeliverIT.Services.Models.InputDTOs;
using DeliverIT.Services.Models.OutputDTOs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Services
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="DeliverIT.Services.Interfaces.IParcelService" />
    public class ParcelService : IParcelService
    {
        /// <summary>
        /// The database context
        /// </summary>
        private readonly IDeliverITDbContext dbContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="ParcelService"/> class.
        /// </summary>
        /// <param name="dbContext">The database context.</param>
        public ParcelService(IDeliverITDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        /// <summary>
        /// Gets parcel with specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <exception cref="ParcelNotFoundException"></exception>
        public ParcelDTO Get(int id)
        {
            var parcel = GetIncludedParcels().FirstOrDefault(p => p.ParcelId == id);

            if (parcel == null)
            {
                throw new ParcelNotFoundException(id);
            }

            return new ParcelDTO(parcel);
        }

        /// <summary>
        /// Gets all parcels.
        /// </summary>
        /// <returns>
        /// Collection of all parcels
        /// </returns>
        public IEnumerable<ParcelDTO> GetAll()
        {
            var parcels = GetIncludedParcels();

            return GetParcelDTOs(parcels);
        }

        /// <summary>
        /// Register the specified parcel.
        /// </summary>
        /// <param name="inputParcel">The input parcel.</param>
        /// <returns>
        /// The registered parcel as ParcelDTO object
        /// </returns>
        /// <exception cref="ArgumentException"></exception>
        public ParcelDTO Create(InputParcel inputParcel)
        {
            if (inputParcel == null)
            {
                throw new ArgumentNullException(string.Format(ErrorMessages.FAILED_REGISTRATION, "model"));
            }

            var parcelToAdd = new Parcel();

            this.ValidateInput(inputParcel);
            this.SetState(parcelToAdd, inputParcel);

            dbContext.Parcels.Add(parcelToAdd);
            dbContext.SaveChanges();

            return new ParcelDTO(parcelToAdd);
        }

        /// <summary>
        /// Deletes parcel with specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// true if the parcel is successfully deleted or false otherwise
        /// </returns>
        public bool Delete(int id)
        {
            var parcel = dbContext.Parcels.FirstOrDefault(p => p.ParcelId == id);

            if (parcel == null)
            {
                return false;
            }

            dbContext.Parcels.Remove(parcel);
            dbContext.SaveChanges();

            return true;
        }

        /// <summary>
        /// Updates parcel with specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="inputParcel">The parcel.</param>
        /// <returns>
        /// The updated parcel as ParcelDTO object
        /// </returns>
        public ParcelDTO Update(int id, InputParcel inputParcel)
        {
            if (inputParcel == null)
            {
                throw new ArgumentNullException();
            }

            var parcelToUpdate = GetIncludedParcels().FirstOrDefault(p => p.ParcelId == id);

            if (parcelToUpdate == null)
            {
                throw new ParcelNotFoundException(id);
            }

            this.ValidateInput(inputParcel);
            this.UpdateState(parcelToUpdate, inputParcel);

            dbContext.SaveChanges();

            return new ParcelDTO(parcelToUpdate);
        }
        /// <summary>
        /// Filters the parcels by customer.
        /// </summary>
        /// <param name="firstName">The first name.</param>
        /// <param name="lastName">The last name.</param>
        /// <returns>Collection of the filtered by customer parcels as ParcelDTO objects</returns>
        public IEnumerable<ParcelDTO> FilterByCustomer(string firstName, string lastName)
        {
            IEnumerable<Parcel> parcels = GetIncludedParcels();

            if (firstName != null)
            {
                parcels = parcels.Where(p => p.Purchaser.FirstName.ToLower() == firstName.ToLower());
            }

            if (lastName != null)
            {
                parcels = parcels.Where(p => p.Purchaser.LastName.ToLower() == lastName.ToLower());
            }

            return GetParcelDTOs(parcels);
        }

        /// <summary>
        /// Filters the parcels by warehouse.
        /// </summary>
        /// <param name="city">The city.</param>
        /// <param name="country">The country.</param>
        /// <returns>Collection of the filtered parcels by warehouse as ParcelDTO objects</returns>
        /// <exception cref="ArgumentException">
        /// There is no warehouses in this city
        /// or
        /// There is no warehouses in this country
        /// or
        /// We don't have warehouses in this city. "Check other available warehouses in {validCountry.Name}"
        /// </exception>
        public IEnumerable<ParcelDTO> FilterByWarehouse(string city, string country)
        {
            IEnumerable<Parcel> parcels = GetIncludedParcels();

            if (city != null)
            {
                if (dbContext.Cities.FirstOrDefault(c => c.Name == city) == null)
                {
                    throw new ArgumentException("There is no warehouses in this city");
                }

                parcels = parcels.Where(p => p.Warehouse.Address.City.Name == city);
            }

            if (country != null)
            {
                var validCountry = dbContext.Countries.Include(c => c.Cities).FirstOrDefault(c => c.Name == country);

                if (validCountry == null)
                {
                    throw new ArgumentException("There is no warehouses in this country");
                }

                if (city != null)
                {
                    if (validCountry.Cities.FirstOrDefault(c => c.Name == city) == null)
                    {
                        throw new ArgumentException($"We don't have warehouses in this city. " +
                            $"Check other available warehouses in {validCountry.Name}");
                    }
                }

                parcels = parcels.Where(p => p.Warehouse.Address.City.Country.Name == country);
            }

            return GetParcelDTOs(parcels);
        }

        /// <summary>
        /// Filters the parcels by category.
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <returns>Collection of the filtered parcels by category as ParcelDTO objects</returns>
        /// <exception cref="CategoryNotFoundException"></exception>
        public IEnumerable<ParcelDTO> FilterByCategory(string categoryName)
        {
            var parcels = GetIncludedParcels();

            if (dbContext.Categories.FirstOrDefault(c => c.Name == categoryName) == null)
            {
                throw new CategoryNotFoundException(categoryName);
            }

            parcels = parcels.Where(p => p.Category.Name == categoryName);

            return GetParcelDTOs(parcels);
        }

        /// <summary>
        /// Filters the parcels by weigth.
        /// </summary>
        /// <param name="toWeight">To weight.</param>
        /// <param name="fromWeight">From weight.</param>
        /// <returns>Collection of the filtered parcels by weight as ParcelDTO objects</returns>
        /// <exception cref="ArgumentException">
        /// Weights musts be positive
        /// or
        /// Invalid weight criterias
        /// </exception>
        public IEnumerable<ParcelDTO> FilterByWeight(double toWeight, double fromWeight = 0)
        {
            var parcels = GetIncludedParcels();

            if (toWeight < 0 || fromWeight < 0)
            {
                throw new ArgumentException("Weights must be positive");
            }

            if (fromWeight > toWeight)
            {
                throw new ArgumentException("Invalid weight criterias");
            }

            parcels = parcels.Where(p => p.Weight >= fromWeight && p.Weight <= toWeight);

            return GetParcelDTOs(parcels);
        }

        /// <summary>
        /// Sorts the by.
        /// </summary>
        /// <param name="sortCriteria">The sort criteria.</param>
        /// <returns>Collection of the sorted parcels as ParcelDTO objects</returns>
        /// <exception cref="ArgumentException">
        /// Invalid sorting order!
        /// or
        /// Invalid sorting criteria!
        /// or
        /// Invalid sorting arguments!
        /// </exception>
        public IEnumerable<ParcelDTO> SortBy(string sortCriteria)
        {
            IEnumerable<Parcel> sortedParcels = GetIncludedParcels();

            if (sortCriteria == null)
            {
                return GetParcelDTOs(sortedParcels);
            }

            var splitted = sortCriteria.Split(',');

            if (splitted.Length > 2)
            {
                throw new ArgumentException("Invalid number of sorting criterias");
            }

            List<List<string>> fullCriteria = new List<List<string>>();

            foreach (var criteria in splitted)
            {

                fullCriteria.Add(criteria.Split('+', ' ').ToList());

            }

            for (int i = 0; i < fullCriteria.Count; i++)
            {
                if (fullCriteria[i].Count == 2)
                {
                    switch (fullCriteria[i][0])
                    {
                        case "weight":
                            switch (fullCriteria[i][1])
                            {
                                case "asc":
                                    sortedParcels = sortedParcels.OrderBy(p => p.Weight);
                                    break;
                                case "desc":
                                    sortedParcels = sortedParcels.OrderByDescending(p => p.Weight);
                                    break;
                                default:
                                    throw new ArgumentException("Invalid sorting order!");
                            }
                            break;
                        case "arrivalDate":
                            switch (fullCriteria[i][1])
                            {
                                case "asc":
                                    sortedParcels = sortedParcels.OrderBy(p => p.Shipment.ArrivalDate);
                                    break;
                                case "desc":
                                    sortedParcels = sortedParcels.OrderByDescending(p => p.Shipment.ArrivalDate);
                                    break;
                                default:
                                    throw new ArgumentException("Invalid sorting order!");
                            }
                            break;
                        default:
                            throw new ArgumentException("Invalid sorting criteria");
                    }
                }
                else if (fullCriteria[i].Count == 1)
                {
                    switch (fullCriteria[i][0])
                    {
                        case "weight":
                            sortedParcels = sortedParcels.OrderBy(p => p.Weight);
                            break;
                        case "arrivalDate":
                            sortedParcels = sortedParcels.OrderByDescending(p => p.Shipment.ArrivalDate);
                            break;
                        default: throw new ArgumentException("Invalid sorting criteria!");
                    }
                }
                else
                {
                    throw new ArgumentException("Invalid sorting arguments!");
                }
            }

            return GetParcelDTOs(sortedParcels);
        }

        /// <summary>
        /// Gets the parcel DTOs.
        /// </summary>
        /// <param name="parcels">The parcels.</param>
        /// <returns>Collection of the parcels as ParcelDTO objects</returns>
        /// <exception cref="ArgumentException"></exception>
        private IEnumerable<ParcelDTO> GetParcelDTOs(IEnumerable<Parcel> parcels)
        {
            var parcelDTOs = new List<ParcelDTO>();

            foreach (var parcel in parcels)
            {
                parcelDTOs.Add(new ParcelDTO(parcel));
            }

            return parcelDTOs;
        }

        /// <summary>
        /// Gets the included parcels.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        private IEnumerable<Parcel> GetIncludedParcels()
        {
            IEnumerable<Parcel> parcels = dbContext.Parcels
                .Include(p => p.Purchaser)
                .Include(p => p.Shipment)
                .Include(p => p.Warehouse)
                   .ThenInclude(w => w.Address)
                      .ThenInclude(a => a.City)
                         .ThenInclude(c => c.Country)
                .Include(p => p.Category);

            return parcels;
        }

        private void ValidateInput(InputParcel inputParcel)
        {
            if (inputParcel.CategoryId != null &&
                dbContext.Categories.FirstOrDefault(c => c.CategoryId == inputParcel.CategoryId.Value) == null)
            {
                throw new CategoryNotFoundException(string.Format(ErrorMessages.INVALID_ID, "Category", inputParcel.CategoryId.Value));
            }

            if (inputParcel.CustomerId != null &&
                dbContext.Customers.FirstOrDefault(c => c.CustomerId == inputParcel.CustomerId.Value) == null)
            {
                throw new CustomerNotFoundException(string.Format(ErrorMessages.INVALID_ID, "Customer", inputParcel.CustomerId.Value));
            }

            if (inputParcel.ShipmentId != null &&
                dbContext.Shipments.FirstOrDefault(s => s.ShipmentId == inputParcel.ShipmentId.Value) == null)
            {
                throw new ShipmentNotFoundException(string.Format(ErrorMessages.INVALID_ID, "Shipment", inputParcel.ShipmentId.Value));
            }

            if (inputParcel.WarehouseId != null &&
                dbContext.Warehouses.FirstOrDefault(w => w.WarehouseId == inputParcel.WarehouseId.Value) == null)
            {
                throw new WarehouseNotFoundException(string.Format(ErrorMessages.INVALID_ID, "Warehouse", inputParcel.WarehouseId.Value));
            }

            if (inputParcel.Weight != null && inputParcel.Weight < 0)
            {
                throw new ArgumentException(ErrorMessages.NEGATIVE_WEIGHT);
            }
        }

        private void UpdateState(Parcel parcelToUpdate, InputParcel parcel)
        {
            if (parcel.CustomerId != null)
            {
                parcelToUpdate.CustomerId = parcel.CustomerId.Value;
            }
            if (parcel.ShipmentId != null)
            {
                parcelToUpdate.ShipmentId = parcel.ShipmentId.Value;
            }
            if (parcel.WarehouseId != null)
            {
                parcelToUpdate.WarehouseId = parcel.WarehouseId.Value;
            }
            if (parcel.CategoryId != null)
            {
                parcelToUpdate.CategoryId = parcel.CategoryId.Value;
            }
            if (parcel.Weight != null)
            {
                parcelToUpdate.Weight = parcel.Weight.Value;
            }
        }

        private void SetState(Parcel parcel, InputParcel inputParcel)
        {
            if (inputParcel.CustomerId == null)
                throw new ArgumentNullException(string.Format(ErrorMessages.PARAMETER_REQUIRED, "Customer id"));

            if (inputParcel.WarehouseId == null)
                throw new ArgumentNullException(string.Format(ErrorMessages.PARAMETER_REQUIRED, "Warehouse id"));

            if (inputParcel.CategoryId == null)
                throw new ArgumentNullException(string.Format(ErrorMessages.PARAMETER_REQUIRED, "Category id"));

            parcel.CustomerId = inputParcel.CustomerId.Value;
            parcel.WarehouseId = inputParcel.WarehouseId.Value;
            parcel.ShipmentId = inputParcel.ShipmentId.Value;
            parcel.CategoryId = inputParcel.CategoryId.Value;
            parcel.Weight = inputParcel.Weight.Value;
        }
    }
}
