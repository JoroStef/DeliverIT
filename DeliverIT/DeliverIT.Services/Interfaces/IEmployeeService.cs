﻿using DeliverIT.Data.Models;
using DeliverIT.Services.Models;
using DeliverIT.Services.Models.InputDTOs;
using DeliverIT.Services.Models.OutputDTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface IEmployeeService
    {
        IEnumerable<EmployeeDTO> GetAll();

        EmployeeDTO Get(int id);

        EmployeeDTO Create(InputEmployee employee);

        EmployeeDTO Update(int id, InputEmployee employee);

        bool Delete(int id);

        Employee AuthenticateByEmail(string email);

        bool IsAuthenticated(string email);
    }
}
