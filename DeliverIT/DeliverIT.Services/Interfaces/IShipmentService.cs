﻿using DeliverIT.Data.Models;
using DeliverIT.Services.Models;
using DeliverIT.Services.Models.InputDTOs;
using DeliverIT.Services.Models.OutputDTOs;
using DeliverIT.Services.QueryObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.Interfaces
{
    public interface IShipmentService
    {
        ShipmentDTO Create(InputShipment shipment);

        IEnumerable<ShipmentDTO> GetAll();

        ShipmentDTO Get(int id);

        ShipmentDTO Update(int id, InputShipment updateShipment);

        bool Delete(int id);

        IEnumerable<ShipmentDTO> FilterByWarehouse(int id);

        IEnumerable<ShipmentDTO> FilterByWarehouse(ShipmentFilteringCriterias criterias);

        IEnumerable<ShipmentDTO> FilterByCustomer(CustomerFilteringCriterias criterias);

        void AddParcel(int shipmentId, int parcelId);

        bool RemoveParcel(int shipmentId, int parcelId);

        int GetOnWayCount();

        string CheckStatus(int shipmentId);

        string CheckShipmentStatusByParcel(int parcelId, int customerId);

        IEnumerable<ParcelDTO> GetCustomerParcelsByStatus(int customerId, string status);

        ShipmentDTO GetNextByWarehouse(int warehouseId);

    }
}
