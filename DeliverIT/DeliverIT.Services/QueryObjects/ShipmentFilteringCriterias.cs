﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.QueryObjects
{
    public class ShipmentFilteringCriterias
    {
        public string Street { get; set; }

        public string City { get; set; }

        public string Country { get; set; }
    }
}
