﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Interfaces;
using DeliverIT.Services.Models;
using DeliverIT.Services.Models.OutputDTOs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Services
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="DeliverIT.Services.Interfaces.ICityService" />
    public class CityService : ICityService
    {
        /// <summary>
        /// The database context
        /// </summary>
        private readonly IDeliverITDbContext dbContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="CityService"/> class.
        /// </summary>
        /// <param name="dbContext">The database context.</param>
        public CityService(IDeliverITDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        /// <summary>
        /// Gets the specified city.
        /// </summary>
        /// <param name="id">The city identifier.</param>
        /// <returns></returns>
        /// <exception cref="CityNotFoundException"></exception>
        public CityDTO Get(int id)
        {
            var city = dbContext.Cities
                .Include(c => c.Country)
                .FirstOrDefault(c => c.CityId == id) ?? throw new CityNotFoundException(id);

            CityDTO cityDTO = new CityDTO(city);

            return cityDTO;
        }

        /// <summary>
        /// Gets all cities.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CityDTO> GetAll()
        {
            var allCities = dbContext.Cities
                .Include(c => c.Country)
                .Select(c => new CityDTO(c));
            
            return allCities;
        }
    }
}
